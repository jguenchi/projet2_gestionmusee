import connexion_perso
import json
connexion = connexion_perso.connexion()
cur = connexion.cursor()

def voir_expos():
    sql = "SELECT e.nom, e.date_debut, e.date_fin FROM EXPOSITION e WHERE ( e.type ='permanente' or (e.date_debut <= current_date ) ) ;"
    cur.execute(sql)
    raw = cur.fetchone()
    print(" Nom de l\'exposition |   DATE DEBUT   |   DATE FIN ")
    while raw:
        print(" '%s'  |  '%s' | '%s' " % (raw[0], raw[1], raw[2]))
        raw = cur.fetchone()


def voir_expo_oeuvres():
    voir_expos()
    exposition = input('Veuillez saisir le nom de l\'exposition : ')
    sql = "SELECT titre, type, auteur FROM Oeuvre WHERE exposition = '%s';" % (exposition)
    cur.execute(sql)
    raw = cur.fetchone()
    print("Titre |   Type d\'oeuvre   |   Auteur ")
    while raw:
        print(" '%s'   |  '%s'  |  '%s' " % (raw[0], raw[1], raw[2]))
        raw = cur.fetchone()


def voir_stat():
    # Prix moyen des oeuvres appartenant au Louvre
    sql1 = "SELECT AVG(o.prix_acquisition) AS Prix FROM Oeuvre o WHERE(o.status = 'prete' OR o.status = 'neutre' );"
    cur.execute(sql1)
    raw = cur.fetchone()
    print("Prix moyen des oeuvres appartenant au Louvre : ", round(raw[0], 2), "€")

    # Prix moyen des oeuvres par types appartenant au Louvre
    sql2 = "SELECT o.type, AVG(o.prix_acquisition) AS Prix FROM Oeuvre o WHERE(o.status = 'prete' OR o.status = 'neutre' ) GROUP BY o.type;"
    cur.execute(sql2)
    raw = cur.fetchone()
    while raw:
        print("Prix moyen des '%s' appartenant au Louvre : " % raw[0], round(raw[1], 2), "€")
        raw = cur.fetchone()

    # Prix moyen des oeuvres par auteur appartenant au Louvre
    sql3 = "SELECT o.auteur, AVG(o.prix_acquisition) AS Prix FROM Oeuvre o WHERE(o.status = 'prete' OR o.status = 'neutre' ) GROUP BY o.auteur;"
    cur.execute(sql3)
    raw = cur.fetchone()
    print("Prix moyen des oeuvres appartenant au Louvre par auteur  : ")
    while raw:
        print(" '%s : " % raw[0], round(raw[1], 2), "€")
        raw = cur.fetchone()

    # Durée moyenne des échanges entre Musées
    sql4 = "SELECT DATE_PART('day', AGE(date_fin, date_debut)) AS duree_moyenne_echange FROM Echange e WHERE e.date_fin IS NOT NULL;"
    cur.execute(sql4)
    raw = cur.fetchone()
    print("Durée moyenne des prêts et emprunts des oeuvres : ", round(raw[0], 2), " jours")


def salles(id_guide):
    jour = input("Veuillez entrer le jour pour lequel vous voulez connaitre les salles à visiter : ")
    sql = "Select es.salle_numero FROM Exposition_Salle es JOIN guide_creneau_exposition gce ON es.exposition = gce.exposition WHERE ( gce.jour = '%s' and gce.guide = '%s' and (  (gce.date_debut IS NULL AND gce.date_fin IS NULL) or (gce.date_debut <= current_date and gce.date_fin <= current_date  ) )) ORDER BY es.salle ;" % (
    jour, id_guide)
    cur.execute(sql)
    raw = cur.fetchone()
    if raw :
        print("Salles pour '%s' " % jour)
        while raw:
            print(raw[0])
            raw = cur.fetchone()
    else :
        print("Aucunes salles pour ce jour")



def edt(id_guide):
    sql = "Select gce.jour, gce.heure_debut, gce.heure_fin, gce.date_debut, gce.date_fin FROM guide_creneau_exposition gce WHERE gce.guide = '%s'; " % (
        id_guide)
    cur.execute(sql)
    raw = cur.fetchone()

    print("JOUR   |    HEURE DE DEBUT    |   HEURE DE FIN   |  DATE DEBUT HORAIRE   |   DATE FIN HORAIRE")
    while raw:
        print(" %s  |    %s        |      %s      |     %s         |         %s " % (
        raw[0], raw[1], raw[2], raw[3], raw[4]))
        raw = cur.fetchone()


def ajout_guide():
    nom = input('Saisir le nom')
    prenom = input('Saisir le nom')
    date_embauche = input("Saisir la date d'embauche ( au format ('YYYY-MM-JJ')")
    sql1 = "INSERT INTO Guide(nom, prenom, date_embauche) VALUES('%s', '%s', '%s');" % (nom, prenom, date_embauche)
    cur.execute(sql1)
    connexion.commit()  # on ajoute le guide dans Guide

    login = input('Saisir le login : ')
    password = input('Saisir le mot de passe : ')

    sql2 = "select id from Guide where nom = '%s' and prenom ='%s' and date_embauche = '%s' ;" % (
    nom, prenom, date_embauche)
    cur.execute(sql2)
    raw = cur.fetchone()  # on cherche l'id du guide qu'on vient d'insérer

    sql3 = "INSERT INTO Utilisateur_connexion(login, password, status, id_guide) VALUES('%s', '%s', 'guide','%s')" % (
    login, password, raw[0])
    cur.execute(sql3)
    connexion.commit()  # on ajoute le guide dans Utilisateur_connexion


def ajout_employe():
    login = input('Saisir le login : ')
    password = input('Saisir le mot de passe : ')
    sql = "INSERT INTO utilisateur_connexion(login, password, status, id_guide) VALUES('%s', '%s', 'employe',NULL)" % (
    login, password)
    cur.execute(sql)
    connexion.commit()


def ajout_admin():
    login = input('Saisir le login : ')
    password = input('Saisir le mot de passe : ')
    sql = "INSERT INTO utilisateur_connexion(login, password, status, id_guide) VALUES('%s', '%s', 'admin',NULL)" % (
    login, password)
    cur.execute(sql)
    connexion.commit()


def ajout_expo():
    nom = input("Saisir le nom de l'exposition : ")
    type_expo = input("Saisir le type d'exposition (permanente/temporaire) : ")

    choix_salle = input("Voulez-vous ajouter des salles ? 1 si oui et 2 sinon ")
    salles = []
    while (choix_salle != '2' ) :
        numero = int(input(" Entrez le numéro de la salle") )
        nombre_max = int( input("Entrez le nombre max de personnes ") )
        dico = {"numero": numero , "nb_max_personnes": nombre_max,"Tableau_explicatif":[]  }
        salles.append(dico)
        choix_tableau = input("Voulez-vous ajouter des tableaux explicatifs pour cette salle ? 1 si oui et 2 sinon ")
        while ( choix_tableau != '2') :
            texte =input("Entrez le texte de votre tableau")
            dico_tab = { "texte": texte }
            salles[-1]["Tableau_explicatif"].append(dico_tab)
            choix_tableau =  input("Voulez-vous ajouter des tableaux explicatifs pour cette salle ? 1 si oui et 2 sinon ")

        choix_salle = input("Voulez-vous ajouter des salles ? 1 si oui et 2 sinon ")
    salles_json = json.dumps(salles)

    if (type_expo == 'permanente'):
        insert_query = "INSERT INTO Exposition (nom , type ,salle) VALUES ( '%s', '%s', '%s')" % (nom, type_expo, salles_json)

    else:
        date_debut = input("Saisir la date de début de l'exposition (format: AAAA-MM-JJ) : ")
        date_fin = input("Saisir la date de fin de l'exposition (format: AAAA-MM-JJ) : ")
        # exécution
        insert_query = "INSERT INTO Exposition (nom , type, date_debut  , date_fin  ,salle) VALUES ( '%s', '%s', '%s','%s','%s')" % (nom, type_expo, date_debut, date_fin, salles_json)

    cur.execute(insert_query)

    # submission
    connexion.commit()

    print("L'exposition a été insérée avec succès dans la base de données.")


def ajout_oeuvre():
    titre = input("Saisir le titre : ")
    date_creation = input("Saisir la date de création (format: AAAA-MM-JJ) : ")
    status = input("Saisir le statut ( prete/emprunte/neutre/tierce ) : ")
    type_oeuvre = input("Saisir le type d'oeuvre : ")

    longueur = float(input("Saisir la longueur : "))
    largeur = float(input("Saisir la largeur : "))
    if (type_oeuvre == 'sculpture' or type_oeuvre == 'Sculpture'):
        hauteur = float(input("Saisir la hauteur : "))
    else:
        hauteur = 'Null'

    if (status == 'prete' or status == 'neutre'):
        prix_acquisition = float(input("Saisir le prix d'acquisition : "))
    else:
        prix_acquisition = 'NULL'

    auteur = input("Saisir l'auteur : ")
    exposition = input("Saisir l'exposition : ")

    # Insertion des données
    insert_query = "INSERT INTO Oeuvre (titre, date_creation, status, longueur, largeur, hauteur, prix_acquisition, type, auteur, exposition)  VALUES ('%s','%s' , '%s', %s, %s, %s, %s, '%s', '%s', '%s')" % (
    titre, date_creation, status, longueur, largeur, hauteur, prix_acquisition, type_oeuvre, auteur, exposition)
    cur.execute(insert_query)

    # submission
    connexion.commit()

    print("L'oeuvre a été insérée avec succès dans la base de données.")


def empruntable(emprunte, id_oeuvre):
    sql = "Select status from Oeuvre WHERE id = %s;" % (id_oeuvre)
    cur.execute(sql)
    raw = cur.fetchone()
    if (raw):
        # si le Louvre prete
        if (not (emprunte)):
            if (raw[0] != 'neutre'):
                return False
            else:
                return True
        else:
            if (raw[0] != 'tierce'):
                return False
            else:
                return True

    else:
        return False


def ajout_echange():
    emprunte = input('Tapez : \n 1 si le Louvre emprunte \n 2 si le Louvre prête \n')
    while (not (emprunte in ['1', '2'])):
        emprunte = input('Tapez : \n 1 si le Louvre emprunte \n 2 si le Louvre prête')

    if (emprunte == '1'):
        emprunte = True
    else:
        emprunte = False

    if (emprunte):
        id_musee = input('Veuillez saisir l identifiant du musée prêteur : ')
        id_oeuvre = input('Veuillez saisir l identifiant de l\'oeuvre empruntée : ')
        print('Les dates doivent être écrites sous le format YYYY-MM-DD')
        date_debut = input('Quelle est la date du début de l emprunt ?  ')
        date_fin = input('Quelle est la date de fin de l emprunt ?  ')
    else:
        id_musee = input('Veuillez saisir l identifiant du musée emprunteur : ')
        id_oeuvre = input('Veuillez saisir l identifiant de l oeuvre prêtée : ')
        print('Les dates doivent être écrites sous le format YYYY-MM-DD')
        date_debut = input('Quelle est la date du début du prêt ?  ')
        date_fin = input('Quelle est la date de fin du prêt ?  ')

    if (empruntable(emprunte, id_oeuvre)):
        sql = "INSERT INTO Echange(musee,oeuvre,emprunte,date_debut,date_fin) VALUES( %s, %s, %s , '%s', '%s' );" % (
        id_musee, id_oeuvre, emprunte, date_debut, date_fin)
        cur.execute(sql)
        connexion.commit()
        if (emprunte):
            sql = "UPDATE Oeuvre SET status = 'emprunte' WHERE id = %s;" % (id_oeuvre)
        else:
            sql = "UPDATE Oeuvre SET status = 'prete' WHERE id = %s;" % (id_oeuvre)

        cur.execute(sql)
        connexion.commit()


    else:
        print("Cet échange ne peut pas être effectué, vérifier que l appartenance de l oeuvre et son statut actuel")


def supp_oeuvre(id):  # supprime grâce à l'id d'une oeuvre celle ci correctement
    sql1 = "delete from Visite where creneau in (select id from Creneau where exposition in(select nom from Exposition  where id = '%s' ) );" % (
        id)
    sql2 = "delete from restauration where oeuvre in (select id from oeuvre where id = '%s');" % (id)
    sql3 = "delete from echange where oeuvre in (select id from oeuvre where id = '%s' );" % (id)
    sql4 = "delete from oeuvre where id = '%s'; " % (id)
    cur.execute(sql1)
    cur.execute(sql2)
    cur.execute(sql3)
    cur.execute(sql4)
    connexion.commit()  # Application dans la base de données


def supp_expo():
    noms = []
    noms.append('0')
    choix = -1  # D'abord on va afficher toutes les expositions disponibles
    print("-------------------------------------------------------------------------------")
    print("|********************************** EXPOSITIONS **********************************|")
    print(f"|                                   NOM                                          |")
    sql0 = "select nom from exposition;"
    cur.execute(sql0)
    raws = cur.fetchall()
    if raws:
        for raw in raws:
            noms.append(raw[0])
            print(f"|            {raw[0]}                               |")  # On affiche le nom de l'exposition
    while (choix not in noms) :
        choix = input("Choisir le nom de l'exposition à supprimer, 0 pour quitter")
    if choix == '0':
        return  # On sort de la fonction si choix vaut 0

    # Suppression de l'exposition de nom = choix

    sql1 = "delete from exposition_salle where exposition =  '%s' ;" % (choix)
    cur.execute(sql1)

    sql2 = "delete from visite where exposition =  '%s';" % (choix)
    cur.execute(sql2)

    sql3 = "select id from oeuvre where exposition =  '%s';" % (
        choix)  # BESOIN DE DELETE TOUTES LES OEUVRES POUR UNE EXPOSITION
    cur.execute(sql3)

    oeuvres = cur.fetchall()  # Toutes les id liées à l'exposition
    for oeuvre in oeuvres:
        id = oeuvre
        supp_oeuvre(id)  # supprime de maniere reglo toutes les oeuvres de l'exposition

    sql4 = "delete from tableau_explicatif where exposition = '%s'; " % (choix)
    cur.execute(sql4)

    sql5 = "delete from exposition where nom = '%s';" % (choix)
    cur.execute(sql5)
    connexion.commit()  # Application dans la base de données

