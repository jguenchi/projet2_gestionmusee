SELECT * FROM Oeuvre
WHERE artiste = 'L.Vinci' ;

SELECT * FROM Oeuvre o
JOIN  Exposition e ON e.nom = o.exposition
WHERE e.type = 'temporaire' ;


/* Prix moyens des oeuvres, des oeuvres par type et par artiste */
SELECT  AVG(o.prix_acquisition) AS Prix FROM Oeuvre o
WHERE (o.status = 'prete'  OR o.status = 'neutre' ) ;

SELECT o.type, AVG(o.prix_acquisition) AS Prix FROM Oeuvre o
WHERE (o.status = 'prete'  OR o.status = 'neutre' )
GROUP BY o.type ;

SELECT o.auteur, AVG(o.prix_acquisition) AS Prix FROM Oeuvre o
WHERE (o.status = 'prete'  OR o.status = 'neutre' )
GROUP BY o.auteur ;

select * from oeuvre ;


/* Oeuvres en cours d emprunt ou de prêt */
SELECT * FROM Echange
WHERE (date_fin >= CURRENT_DATE) AND (emprunte = True );
SELECT * FROM Echange
WHERE (date_fin >= CURRENT_DATE) AND (emprunte = False );

/* Durée moyenne des échanges entre Musées */
SELECT DATE_PART('day', AGE(date_fin, date_debut))  AS duree_moyenne_echange
FROM Echange e
WHERE e.date_fin IS NOT NULL;


/* Toutes les oeuvres reliées aux créneaux permanents */
SELECT o.titre FROM Oeuvre o
JOIN Exposition e ON e.nom = o.exposition
JOIN Visite v ON v.exposition = e.nom
JOIN Creneau c ON v.creneau = c.id
WHERE c.type = 'permanent' ;


/* Toutes les oeuvres vues pour le guide 1 dans ses créneaux */
SELECT o.titre FROM Oeuvre o
JOIN Exposition e ON e.nom = o.exposition
JOIN Visite v ON v.exposition = e.nom
JOIN Creneau c ON v.creneau = c.id
JOIN Guide g ON g.id = c.guide
WHERE g.id = 1 ;


/* Tranche horaire n étant attribuées à aucun Guide */
SELECT t.heure_debut, t.heure_fin FROM Tranche_horaire t
LEFT JOIN Creneau c ON c.horaire = t.id
WHERE (c.id IS NULL ) ;



/* View des salles et des tableaux explicatifs */
CREATE VIEW Exposition_salle AS
 select
    nom,
    JSON_ARRAY_ELEMENTS(salle)->'numero' AS salle_numero,
JSON_ARRAY_ELEMENTS(JSON_ARRAY_ELEMENTS(salle)->'Tableau_explicatif')->>'texte' as tableau_texte
from exposition;





select * from Exposition_salle ;

select * from Exposition ;

/* Les salles d une exposition */

select s.numero from Salle s
join Exposition_Salle es on s.numero = es.salle
where es.exposition =  'Napoleon';


/* Les Guides ne travaillant pas le week-end */

SELECT g.nom FROM Guide g
EXCEPT
SELECT g.nom FROM Guide g
JOIN Creneau c ON g.id = c.guide
WHERE c.jour = 'Samedi' OR c.jour =  'Dimanche';


/* Le nombre de Guide différent travaillant chaque jour de la semaine (GROUP BY) */
SELECT c.jour, COUNT(DISTINCT g.id) AS nombre_guides
FROM Guide g
JOIN Creneau c ON g.id = c.guide
GROUP BY c.jour;


/* Tous les Guides travaillant le lundi ou mardi ou mercredi sur une certaine exposition */

SELECT DISTINCT g.nom, g.prenom
FROM Guide g
JOIN Creneau c ON g.id = c.guide
JOIN Visite v ON c.id = v.creneau
JOIN Exposition e ON v.exposition = e.nom
WHERE (c.jour = 'Lundi' OR c.jour = 'Mardi' OR c.jour = 'Mercredi') AND e.nom = 'Napoleon';

/* Le nombre de tableau explicatif pour chaque type d’exposition (combien sont dans les expositions permanentes et combien dans les expositions temporaire actuellement) */

SELECT COUNT(Tableau_explicatif.numero)
FROM Tableau_explicatif
JOIN Exposition ON Tableau_explicatif.exposition = Exposition.nom
WHERE Exposition.type = 'permanente';

SELECT COUNT(Tableau_explicatif.numero)
FROM Tableau_explicatif
JOIN Exposition ON Tableau_explicatif.exposition = Exposition.nom
WHERE Exposition.type = 'temporaire';




# View pour les informations sur une oeuvre
CREATE VIEW oeuvre_auteur AS
SELECT o.* , a.nom as Nom_auteur, a.prenom as Prenom_auteur , a.date_naissance as Naissance_auteur, a.date_deces as Deces_auteur
FROM Oeuvre o
JOIN Auteur a ON a.nom_artiste = o.auteur
;

select * from oeuvre_auteur;

# View emploi du temps des guides
CREATE VIEW guide_creneau_exposition AS
SELECT c.* , g.nom as Guide_nom, g.prenom as Guide_prenom, t.heure_debut, t.heure_fin, e.nom as Exposition, e.type as Type_Exposition
FROM Creneau c
JOIN Guide g ON c.guide = g.id
JOIN Tranche_horaire t ON t.id = c.horaire
JOIN Visite v ON v.creneau = c.id
JOIN Exposition e ON e.nom = v.exposition
WHERE ( (c.type = 'permanent') or ( c.date_fin >= current_date) )
;

select * from guide_creneau_exposition;

# View des salles et des tableaux explicatifs
CREATE VIEW exposition_salle_tableau AS
SELECT  e.* , s.*, t.numero AS Numero_tableau, t.texte as Texte_tableau
FROM Exposition e
JOIN Exposition_Salle es ON  es.exposition = e.nom
JOIN Salle s ON  es.exposition = e.nom
JOIN Tableau_explicatif t ON  t.salle = s.numero
;

select * from exposition_salle_tableau;




/* Les salles d une exposition */
SELECT JSON_ARRAY_ELEMENTS(salle)->'numero' AS numero
FROM Exposition
WHERE nom = 'Sculptures Grecques';


/* Le nombre de tableau explicatif pour chaque type d’exposition (combien sont dans les expositions permanentes et combien dans les expositions temporaire actuellement) */
pas dans l’app python pas de besoin de modification



