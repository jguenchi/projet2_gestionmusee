/* Contrainte pour Visite et Echange sur le type de l'oeuvre ou le type de l'exposition à insérer lors de l'insertion en python dans les tables')
  En effet, nos insertions sont des exemples construit de tel sorte qu'il n'y ait aucunes erreurs, nous gérerons les contraintes d'échange et de Visite correctement en python*/


CREATE TABLE Guide(
id serial PRIMARY KEY not null ,
nom VARCHAR(100) not null,
prenom VARCHAR(100) not  null,
date_embauche DATE not null
);


CREATE TABLE Utilisateur_connexion (
    id serial PRIMARY KEY ,
    login varchar NOT NULL,
    password varchar NOT NULL,
    status  varchar NOT NULL Check(status in ('admin','employe','guide') ) ,
    id_guide INT  REFERENCES Guide(id),
   CHECK(  (id_guide IS NOT NULL and status = 'guide')  or  (id_guide IS NULL and status != 'guide')      )
) ;





CREATE TABLE Musee (
id serial PRIMARY KEY ,
adresse Varchar(100) not null,
nom Varchar(100) not null ,
pays Varchar(50) not null
);




CREATE TABLE Auteur(
nom_artiste VARCHAR(100) PRIMARY KEY,
nom VARCHAR(50)NOT NULL,
prenom VARCHAR(50) NOT NULL,
date_naissance DATE NOT NULL,
date_deces DATE
);




/*exposition*/
create table Exposition (
   nom varchar(50) primary key ,
   type varchar not null check (type in ('permanente', 'temporaire')),
   date_debut date ,
   date_fin date ,
   salle Json,
   check( (type = 'permanente' and date_debut is NULL and date_fin is NULL)
  or (type = 'temporaire' and date_debut is not NULL and date_debut <= date_fin )
)
);


/*oeuvre*/
CREATE TABLE Oeuvre (
id serial PRIMARY KEY,
titre VARCHAR(50) NOT NULL ,
date_creation DATE NOT NULL,
status varchar not null check( status in('prete', 'emprunte', 'neutre','tierce')),
longueur float NOT NULL,
largeur float NOT NULL,
hauteur float,
prix_acquisition FLOAT CHECK(prix_acquisition >=0),
type varchar not null check (type in ('peinture', 'photographie', 'sculpture')),
auteur VARCHAR(100),
exposition VARCHAR(100),
Restauration Json,
FOREIGN KEY (auteur) REFERENCES Auteur(nom_artiste),
FOREIGN KEY (exposition) REFERENCES Exposition(nom)
) ;








create table Tranche_horaire (
   id serial primary key,
   heure_debut time  not null,
   heure_fin time not null,
   unique(heure_debut, heure_fin)
);




create table Creneau (
   id serial primary key,
   guide integer not null references Guide(id),
   horaire integer not null references Tranche_horaire(id),
   jour varchar not null check(jour in ('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche')),
   type varchar not null check(type in ('exceptionnel', 'permanent')),
   date_debut date,
   date_fin date,
   unique (guide, horaire , jour),
   check (
   (type = 'permanent' and date_debut is null and date_fin is null)
   or (type = 'exceptionnel' and date_debut is not null and date_fin is not null and date_debut <= date_fin)
 )
);


create table Visite (
   id serial primary key,
   creneau integer not null references Creneau(id),
   exposition varchar not null references Exposition(nom),
   /*statut_expo varchar not null references Exposition(type),
   statut_creneau varchar not null references Creneau(type),
   check((statut_expo = 'temporaire' and statut_creneau = 'exceptionnel')or(statut_expo = 'permanente' and statut_creneau = 'permanent') ),*/
   unique (creneau, exposition)
);




CREATE TABLE Echange(
musee integer not null REFERENCES Musee(id),
oeuvre INTEGER not null REFERENCES Oeuvre(id),
emprunte boolean NOT NULL,
/*status_oeuvre varchar not null references Oeuvre(status),
check((status_oeuvre = 'neutre' and emprunte = 'False')or(status_oeuvre = 'tierce' and emprunte = 'true') ),*/
date_debut date not null,
date_fin date not null,
PRIMARY KEY(musee, oeuvre,emprunte),
check ( date_debut <= date_fin  )
                    );


/*CHECK (oeuvre = (select id from oeuvre where (status = 'neutre' or status = 'tierce' and oeuvre = id)) )  Si oeuvre n'est pas neutre ou tierce alors on ne peut pas insérer */



