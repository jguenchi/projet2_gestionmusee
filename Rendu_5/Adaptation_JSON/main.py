import connexion_perso
import json
from fonctions import *
connexion = connexion_perso.connexion()
cur = connexion.cursor()

def menu_principal() :
    print("-------------------------------------------------------------------------------")
    print("|************************************ MENU ***********************************|")
    print("|     |*************************** CHOIX POSSIBLES ***************************|")
    print("|  1  | ME CONNECTER ( ATTENTION : EMPLOYES SEULEMENT ! )                     |")
    print("|  2  | VOIR LES OEUVRES / EXPOSITIONS PRESENTES ACTUELLEMENTS                |")
    print("|  3  | QUITTER                                                               |")
    print("-------------------------------------------------------------------------------\n")

def affichage_menu_admin() : # choix = 1
    print("-------------------------------------------------------------------------------")
    print("|************************************ MENU ***********************************|")
    print("|     |*************************** CHOIX POSSIBLES ***************************|")
    print("|  1  | SUPPRIMER UNE EXPOSITION                                              |")
    print("|  2  | SUPPRIMER UNE OEUVRE                                                  |")
    print("|  3  | AJOUTER UN EMPLOYE                                                    |")
    print("|  4  | AJOUTER UN ADMIN                                                      |")
    print("|  5  | AJOUTER UN GUIDE                                                      |")
    print("|  6  | ME DECONNECTER                                                        |")
    print("-------------------------------------------------------------------------------\n")

def affichage_menu_employe() :
    print("-------------------------------------------------------------------------------")
    print("|************************************ MENU ***********************************|")
    print("|     |*************************** CHOIX POSSIBLES ***************************|")
    print("|  1  | AJOUTER UNE EXPOSITION                                                |")
    print("|  2  | AJOUTER UNE OEUVRE                                                    |")
    print("|  3  | AJOUTER UN ECHANGE                                                    |")
    print("|  4  | VOIR LES STATISTIQUES                                                 |")
    print("|  5  | ME DECONNECTER                                                        |")
    print("-------------------------------------------------------------------------------\n")

def affichage_menu_guide() :
    print("-------------------------------------------------------------------------------")
    print("|************************************ MENU ***********************************|")
    print("|     |*************************** CHOIX POSSIBLES ***************************|")
    print("|  1  | VOIR SON EMPLOI DU TEMPS ( DE LA SEMAINE )                            |")
    print("|  2  | VOIR LES SALLES DE LA JOURNEE                                         |")
    print("|  3  | ME DECONNECTER                                                        |")
    print("-------------------------------------------------------------------------------\n")

def menu_guide(id_guide) :
    choix_menu_guide = 0
    while choix_menu_guide != 3:
        affichage_menu_guide()
        try:
            choix_menu_guide = int(input("Choisir entre les 3 options précédentes : "))
        except ValueError:
            print("\n\n ATTENTION : Choisir entre 1 et 3 ")

        if choix_menu_guide == 1 :
            edt(id_guide)  # a faire !
        elif choix_menu_guide == 2 :
            salles(id_guide)  # a faire !


def menu_employe() :
    choix_menu_employe = 0
    while choix_menu_employe != 5:
        affichage_menu_employe()
        try:
            choix_menu_employe = int(input("Choisir entre les 5 options précédentes : "))
        except ValueError:
            print("\n\n ATTENTION : Choisir entre 1 et 5 ")
        if choix_menu_employe == 1 :
            ajout_expo()    # à verifier !
        elif choix_menu_employe == 2 :
            ajout_oeuvre()  # a faire !
        elif choix_menu_employe == 3:
            ajout_echange() # a faire !
        elif choix_menu_employe == 4:
            voir_stat()     # a faire !


def menu_admin() :
    choix_menu_admin = 0
    while choix_menu_admin != 6:
        affichage_menu_admin()
        try:
            choix_menu_admin = int(input("Choisir entre les 6 options précédentes : "))
        except ValueError:
            print("\n\n ATTENTION : Choisir entre 1 et 6 ")
        if choix_menu_admin == 1:
            supp_expo()  # à vérifier !
        elif choix_menu_admin == 2:
            choix_oeuvre = -1  # D'abord on va afficher toutes les oeuvres disponibles
            print("-------------------------------------------------------------------------------")
            print("|********************************** OEUVRES **********************************|")
            print(f"|  ID  |                TITRE          |               STATUT                |")
            sql0 = "select id, titre, status from oeuvre;"
            cur.execute(sql0)
            raws = cur.fetchall()
            if raws:
                for raw in raws:
                    print(f"|  {raw[0]}  | {raw[1]}     | {raw[2]}             |")  # On affiche
            choix_oeuvre = int(input("Choisir l' ID de l'oeuvre à supprimer, 0 pour quitter"))
            while choix_oeuvre < 0 or choix_oeuvre > len(
                    raws):  # len(raws) c'est la valeur maximale courante de ID, si la longueur de raws est 10, alors le dernier id vaut 10 et id commence à 1 car il est de type SERIAL
                choix_oeuvre = int(input("Choisir l' ID de l'oeuvre à  supprimer"))
            if choix_oeuvre == 0:
                return  # On sort de la fonction si choix vaut 0
            supp_oeuvre(choix_oeuvre)  # à vérifier !
        elif choix_menu_admin == 3:
            ajout_employe()  # a faire !
        elif choix_menu_admin == 4:
            ajout_admin()  # a vérifier !
        elif choix_menu_admin == 5:
            ajout_guide()  # à vérifier !




def menu_choix_1(login,password) : # la connexion à la base de donnée
    connect_base = "Select * from Utilisateur_connexion where login = '%s' and password = '%s';" % (login, password)
    cur.execute(connect_base)
    raw = cur.fetchone()
    statut = raw[3]
    if raw:
        if statut == 'guide':
            id_guide = raw[4]
            menu_guide(id_guide)
        elif statut == 'employe':
            menu_employe()
        elif statut == 'admin' :
            menu_admin()
def menu() :
    choix_menu_1 = 0
    while choix_menu_1 != 3 :
        menu_principal()
        try :
            choix_menu_1 = int(input("Choisir entre les 3 options précédentes : "))
        except ValueError :
            print("\n\n ATTENTION : Choisir entre 1 et 3 ")
        if choix_menu_1 == 1 :
            try :
                login = input('Login : ')
                password = input('Password : ')
                menu_choix_1(login,password)
            except TypeError:
                print("ATTENTION : login/password incorrect ! \n")
        if choix_menu_1 == 2 :
            voir_expo_oeuvres()


menu()

cur.close()
connexion.close()

