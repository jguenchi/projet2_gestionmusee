/* Supprimer le guide qui a le nom WAICHINE */

DELETE FROM Visite where creneau in (Select id FROM Creneau WHERE guide IN (SELECT id FROM Guide WHERE nom = 'WAICHINE'));
DELETE FROM Creneau WHERE guide IN (SELECT id FROM Guide WHERE nom = 'WAICHINE');
DELETE FROM Guide WHERE nom = 'WAICHINE';



/* Supprimer l’exposition qui a le nom Chine Empire   */
DELETE FROM Exposition_Salle WHERE exposition = 'Chine Empire';
DELETE FROM Visite WHERE exposition = 'Chine Empire';
DELETE FROM Oeuvre where exposition = 'Chine Empire';
DELETE FROM tableau_explicatif where exposition =  'Chine Empire';
DELETE FROM Exposition WHERE nom = 'Chine Empire';

/* Supprimer toutes les expositions de type "permanent"*/
DELETE FROM Exposition_Salle WHERE exposition IN (SELECT nom FROM Exposition WHERE type = 'permanente');
DELETE FROM Visite WHERE exposition IN (SELECT nom FROM Exposition WHERE type = 'permanente');

DELETE FROM Restauration WHERE oeuvre IN (SELECT oeuvre FROM Oeuvre
                                                          JOIN Exposition e On e.nom = exposition
                                                          where e.type = 'permanente'
                                                              ) ;
DELETE FROM Echange WHERE oeuvre IN (SELECT oeuvre FROM Oeuvre
                                                          JOIN Exposition e On e.nom = exposition
                                                          where e.type = 'permanente'
                                                              ) ;
DELETE FROM OEUVRE WHERE exposition IN (SELECT exposition FROM Oeuvre
                                                          JOIN Exposition e On e.nom = exposition
                                                          where e.type = 'permanente'
                                                              ) ;
DELETE  FROM tableau_explicatif where exposition in (SELECT nom FROM Exposition where type = 'permanente') ;
DELETE FROM Exposition WHERE type = 'permanente';

/* Supprimer toutes les œuvres dont le titre contient "Laché de cigognes" */
DELETE FROM Visite
WHERE creneau IN (
    SELECT id
    FROM Creneau
    WHERE exposition IN (
        SELECT nom
        FROM Exposition
        WHERE nom = 'Laché de cigognes'
    )
);

DELETE FROM Restauration
WHERE oeuvre IN (
    SELECT id
    FROM Oeuvre
    WHERE titre = 'Laché de cigognes'
);

DELETE FROM Echange
WHERE oeuvre IN (
    SELECT id
    FROM Oeuvre
    WHERE titre = 'Laché de cigognes'
);

DELETE FROM Oeuvre
WHERE titre = 'Laché de cigognes';

/* Supprimer la salle de numero 0 */
DELETE FROM Visite
WHERE exposition IN (
  SELECT exposition FROM Exposition_Salle WHERE salle = 0
);
DELETE FROM Exposition_Salle
WHERE salle = 0;
DELETE FROM Salle
WHERE numero = 0;

