Tout les attributs sont non-nuls sauf : hauteur ( dans Oeuvre), date_mort ( dans Auteur ) 

Héritage : - Par la classe mère pour Oeuvre car presque complète malgré qu'elle soit abstraite, le type discriminant ne répertoriera pas 'oeuvre' comme un type possible

Musee(#id : integer, nom : string, ville : string, pays : string )

Auteur(#nom_artiste : string, nom : string,prénom : string,date_naissance : date, date_mort : date ) 

Oeuvre(#id : integer, date : date, statut : {prété|emprunté|neutre}, longueur : float, largeur : float, prix_aquisition : float, type  : {peinture|photographie|sculpture}, hauteur : float, auteur=> Auteur.nom_artiste, expo_tempo=>Exposition_temporaire, expo_perma=>Exposition_permanente) 

avec not(statut = 'emprunté' and expo_perma not null) and not(statut = 'prété' and expo_tempo not null) and (statut = 'emprunté' and expo_tempo not null) and (statut <> 'emprunté' and expo_perma not null)

and not(hauteur not null and type <>'Sculpture') and (hauteur not null and type ='Sculpture')

Emprunte(#musee=>Musee.id,#oeuvre=>oeuvre.id, date_debut : date, date_fin : date) avec oeuvre.statut = 'neutre' // Ici le Louvre emprunte donc le statut va avec, il passera a 'emprunté'

Prête(#musee=>Musee.id,#oeuvre=>oeuvre.id date_debut : date, date_fin : date) avec oeuvre.statut = 'neutre' // Ici le Louvre prête donc le statut va avec, il passera a 'preté'

Restauration(#numero : integer,oeuvre=>oeuvre.id, date_debut : date, date_fin : date) avec not(oeuvre.statut = 'prété' ) 



