
###########################################    ADMIN ###########################

# AJOUT D UN GUIDE
INSERT INTO Guide (nom, prenom, date_embauche)
VALUES ('WAICHINE', 'Nathalie', '2018-03-04');

# AJOUT D UN COMPTE UTILISATEUR GUIDE
INSERT INTO Utilisateur_connexion (login, password, status, id_guide ) VALUES
('jacques', 'jj', 'guide', 1)
;



DELETE FROM Visite
WHERE creneau IN (
   SELECT id
   FROM Creneau
   WHERE exposition IN (
       SELECT nom
       FROM Exposition
       WHERE id = 1
   )
);

DELETE FROM Restauration
WHERE oeuvre IN (
   SELECT id
   FROM Oeuvre
   WHERE id = 1
);

DELETE FROM Echange
WHERE oeuvre IN (
   SELECT id
   FROM Oeuvre
   WHERE id = 1
);

DELETE FROM Oeuvre
WHERE id = 1;





###########################################    EMPLOYE ###########################

# AJOUTER UNE EXPOSITION PERMANENTE
INSERT INTO Exposition VALUES
('Chine Empire', 'permanente' ,null, null)  ;
# Expo temporaire
INSERT INTO Exposition VALUES
('Peintures espagnol au XIXeme siecle', 'temporaire' ,'2022-02-01', '2023-05-12');

# AJOUTER UNE OEUVRE 
# NON SCULPTURE
INSERT INTO Oeuvre (titre, date_creation, status, longueur, largeur, hauteur, prix_acquisition, type, auteur, exposition)
VALUES ('Laché d oiseaux', '1976-11-10', 'neutre', 16.5, 20.3, null, 500, 'peinture', 'Banksy', 'Art contemporain' );

# SCULPTURE
INSERT INTO Oeuvre (titre, date_creation, status, longueur, largeur, hauteur, prix_acquisition, type, auteur, exposition)
VALUES ('Laché d oiseaux', '1976-11-10', 'neutre', 16.5, 20.3, 53.5, 500, 'peinture', 'Banksy', 'Art contemporain' );


# AJOUTER UN ECHANGE 

INSERT INTO Echange(musee,oeuvre,emprunte,date_debut,date_fin)
VALUES 
(1, 1, false,'2022-05-16','2024-02-12'),
(2, 2, true,'2021-05-16','2024-04-30')
;





###########################################    GUIDE   ###########################
# AFFICHER EMPLOI DU TEMPS D UN GUIDE
Select gce.jour,  gce.heure_debut, gce.heure_fin , gce.date_debut, gce.date_fin
FROM  guide_creneau_exposition gce
WHERE gce.guide_id = 1 ; 

# AFFICHER LES SALLES  DUN GUIDE POUR SA JOURNEE
Select es.numero FROM Exposition_Salle es
JOIN guide_creneau_exposition gce ON es.exposition = gce.exposition
WHERE ( gce.jour = 'Lundi' and gce.guide = 1 and 
(  (gce.date_debut IS NULL AND gce.date_fin IS NULL) or (gce.date_debut <= current_date and gce.date_fin <= current_date  ) ) 
) ;



###########################################  VISITEUR ###########################

# VOIR LES EXPOSITIONS ACTUELLES 
SELECT e.nom, e.date_debut, e.date_fin FROM EXPOSITION e
WHERE ( e.type ='permanent' or (e.date_debut <= current_date ) ) ;


# LISTE DES OEUVRES AU LOUVRE ACTUELLEMENT 
SELECT *  FROM Oeuvre  WHERE Oeuvre.status != 'tierce' ;

# VOIR LES OEUVRES POUR UNE EXPOSITION DONNEE
SELECT * FROM Oeuvre 
WHERE exposition = 'Napoleon' ;


