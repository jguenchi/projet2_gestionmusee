NDC :


Oeuvre peut être soit une peinture soit une scuplture soit une photographie et ne peut pas être deux de ces catégories à la fois. Une oeuvre ne peut pas non plus être juste une oeuvre. Nous avons donc une classe abstraite avec héritages et trois classes filles (peinture, sculpture et photographie). 

Attributs Oeuvre :
Oeuvre a pour attributs titre, date, longueur, largeur dans la classe mère. Elle aura également profondeur dans le cas d'une sculpture. La classe mère a également l'attribut status. Il y a trois status, prêté, emprunté ou neutre dans le cas où l'oeuvre n'est ni prêtée ni empruntée.

Pour les Oeuvre appartenant au Louvre nous voulons être capable de calculer le prix moyens de ces oeuvres.
Nous voulons également calculer la proportions d'Oeuvre étant des peintures, des photographies ou des sculpture appartenant au Louvre.


Associations Oeuvre :
La classe mère aura également une relation avec Auteur puisque qu'une oeuvre a exactement un Auteur. 
Une oeuvre est dans exactement une exposition. Si l'oeuvre appartient au Louvre alors son exposition doit être permanente et si l'oeuvre appartient à un autre musée alors l'exposition doit être temporaire. 
Une oeuvre peut ou non être en Restauration lorsqu'elle appartient au Louvre et qu'elle n'est actuellement pas prêtée. 
Une oeuvre est possedée par exactement un musée. 
Une oeuvre peut être transférée (empruntée ou prêtée) aucune ou plusieurs fois  à un autre Musée que le Louvre.

--

Restauration liste les oeuvres appartenants au Louvre qui ne seront pas physiquement leur exposition associée. Ce tableau a également les attributs date de debut et date de fin pour la durée d'indisponibilité.

Restauration a une association uniquement avec les oeuvres  appartenant au Louvre et non prêtées. Une Restauration est associée a exactement une Oeuvre et une Oeuvre peut être plusieurs fois en Restauration.

--

Musee liste les musées partenaire ou potentiel partenaire du Louvre (Louvre inclus). On a comme attributs, adresse, nom , ville et pays. 
Un Musee possède aucune ou plusieurs Oeuvre. 
Un Musee peut prêter/emprunter aucune ou plusieurs Oeuvre mais pas plusieurs fois la même.

--

Echange liste les transfères d'oeuvres entre les musées. C'est une classe d'association entre Musee et Oeuvre. Les attributs sont date de début de l'échange, la date de fin et un booléen emprunte afin de connaitre la nature de l'échange ( vrai si emprunté, faux si prété). La clé sera le couple d'id de l'oeuvre, celle du musée ainsi que la valeur de l'attribut emprunte pour éviter qu'on ne puisse prêter/emprunter plusieurs fois la même oeuvre. 
A chaque Echange est associé exactement une Oeuvre et exactement un Musee. Un Musee peut apparaitre aucune ou plusieurs fois et une Oeuvre également.

Nous voulons être capable de calculer la durée moyenne des prêts ou emprunts d'oeuvres.
Nous voulons calculer la proportions des peintures, sculpture et photographies parmis les oeuvres prêtées et empruntées par le Louvre.

--

Auteur a pour attributs son nom, prenom, nom d'artiste, date de naissance et date de décès. La date de décès est facultative. L'auteur a crée aucune ou des Oeuvre.

--

Exposition doit être temporaire ou permanente. On a donc une classe mère abstraite et deux classes filles. L'héritage est exclusif puisque il n'existe pas d'exposition permanente et temporaire. 
La classe mère a pour attribut le nom qui est la clé. Elle a également une méthode pour connaitre le prix moyen des oeuvres d'une exposition. 

Nous voulons être capable de calculer la durée moyenne des expositions temporaire.

Associations Exposition
Une exposition est composée d'au moins une Oeuvre et potentiellement plusieurs. Une exposition Temporaire est composée uniquement d'Oeuvres n'appartenant pas au Louvre. Une Exposition Permanente est composée uniquement d'Oeuvres appartenant au Louvre.
Une exposition est exposée dans au moins une Salle et potentiellement plusieurs.
Une Exposition est associée a aucun ou plusieurs Tableau Explicatif.

--

Salle a pour attribut un numero qui est sa clé et un nombre maximum de personne.
Une Salle est associée a aucune ou plusieurs exposition (dans le cas d'une grande salle plusieurs expositions peuvent partager cette salle). 
Une salle est également associée a aucun ou plusieurs Tableaux explicatifs.

-- 

Tableau explicatif a pour attributs un numero qui est sa clé et un texte. 
Il est associé a exactement une Exposition et a exactement une Salle.

-- 

Guide a pour attributs un identificateur qui est sa clé, un nom, prenom et une date d'embauche. 
Un Guide est associé a aucun ou plusieurs Créneaux (temporaire ou usuel) ce qui correspond à son emploi du temps. 

--

Tranche_horaire a pour attribut une heure de début et une heure de fin. Elle est associée a aucun ou plusieurs Creneaux. 

--

Creneau a pour attribut un jour de la semaine. Il est associé a exactement une Tranche Horaire et a exactement un Guide. Un Creneau peut être usuel ou temporaire. Le creneau usuel est celui qui se reproduit chaque semaine. Le Creneau Temporaire aura en plus comme attributs une date de début et de fin. On a donc un hértiage exclusif puisque qu'un créneau est soit temporaire soit usuel mais jamais les deux.

Un Creneau est toujours associé au moins une voir plusieurs expositions. L'exposition ne peut être que permanente si le créneau est usuel et que temporaire si le créneau est temporaire.
--



Nous prévoyons d'avoir plusieurs rôles pour notre base de données. 

Des administrateurs seront nécessaires et seront les seuls à pouvoir supprimer des oeuvres ou des Musée par exemple. Ils pourront tout faire dans la base de données.

Des employés organisateurs seront capable d'ajouter de nouveaux musées partenaires et d'également ajouter des échanges au tableau Echange d'oeuvres ainsi que de voir et modifier l'association des oeuvres avec leurs expositions, leurs status...Ils pourront également ajouter de nouvelles expositions et en supprimer. Ils ne pourront cependant pas supprimer d'Oeuvre ni de Musée.

Le rôle Guides sera capable de regarder leurs créneaux et le créneau des autres guides mais ne pourra rien supprimer ou ajouter à la base de données. 

Le rôle visiteur permettra d'afficher l'ensemble des oeuvres et des expositions mais sans pouvoir les modifier ni ajouter quoi que ce soit à la base de données.









