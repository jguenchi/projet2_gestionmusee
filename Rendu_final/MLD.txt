Tous les attributs sont non-nuls sauf : hauteur ( dans Oeuvre), date_mort ( dans Auteur ) 

Héritage : - Par la classe mère pour Oeuvre, Exposition et Creneau car presque complète malgré qu'elles soient abstraites, les différents types discriminants ne répertorieront pas 'oeuvre', 'exposition' ou 'creneau' comme un types possibles

Musee(#id : integer, adresse : string, nom : string, ville : string, pays : string )

Auteur(#nom_artiste : string, nom : string,prénom : string, date_naissance : date, date_mort : date ) Avec date_mort Nullable

Oeuvre(#id : integer, titre : string, date : date, status : {prété|emprunté|neutre}, longueur : float, largeur : float, prix_aquisition : float, type  : {peinture|photographie|sculpture}, hauteur : float, auteur=> Auteur.nom_artiste, exposition => Exposition.nom) 
Avec hauteur Nullable et avec Projection(Oeuvre,auteur) ⊆ Projection(Auteur,nom_artiste) et avec Projection(Oeuvre,exposition) = Projection(Exposition,nom)
CHECK( (hauteur != null and type == sculpture) or (hauteur == null and type != sculpture)  )


Restauration(#numero : integer,oeuvre=>oeuvre.id, date_debut : date, date_fin : date) avec not(oeuvre.statut = 'prété' ) et Projection(Restauration,oeuvre) ⊆ Projection(Oeuvre,id)

Echange(#musee=>Musee.id, #oeuvre=>oeuvre.id , #emprunte : boolean
date_debut : date, date_fin : date) avec Projection(Echange,musee) ⊆ Projection(Musee,id) et Projection(Echange,oeuvre) ⊆ Projection(Oeuvre,id)

Si emprunte = True alors Check( oeuvre.id != emprunte and oeuvre.id != preté) et oeuvre.status passe a 'emprunté'
Si emprunte = False alors Check( oeuvre.status= Neutre) et oeuvre.status devient 'preté'


Salle(#numero : int, nb_max_personnes : int ) 


Tableau_explicatif(#numero : int , texte : string, exposition => Exposition.nom ) avec Projection(Tableau_explicatif,exposition) ⊆ Projection(Exposition,nom) et Projection(Tableau_explicatif,salle) ⊆ Projection(Salle,numero)

Exposition(#nom : string, type : {permanente|temporaire}, date_debut : date, date_fin : date )
Avec date_debut, date_fin NULLABLE
Check( (type == permanente and date_debut == NULL and date_fin == NULL) 
	XOR (type == temporaire and date_debut != NULL and date_debut <= date_fin ) 
) 

Exposition_Salle(#exposition => Exposition.nom, #salle => Salle.numero) avec Projection(Exposition_Salle,salle)  ⊆  Projection(Salle,numero)   et Projection(Exposition,nom) = Projection(Exposition_Salle,exposition)

Guide(#id : int, nom: string, prenom : string, date_embauche : date)


Tranche_horaire(#heure_debut : time, #heure_fin : time  )


Creneau( #id : int, guide => Guide.id, heure_debut => Tranche_horaire.heure_debut, 
heure_fin => Tranche_horaire.heure_fin , 
    jour : {Lundi|Mardi|Mercredi|Jeudi|Vendredi|Samedi|Dimanche } ,
type : {exceptionnel | permanent } ,
date_debut : date, date_fin : date   )

Avec date_debut, date_fin NULLABLE
(guide, heure_debut, heure_fin, jour) Unique
Check( (type == permanente and date_debut == NULL and date_fin == NULL) 
	XOR (type == temporaire and date_debut != NULL and date_debut <= date_fin ) 
) et Projection(Creneau,guide) ⊆ Projection(Guide,id) et Projection(Creneau,heure_debut, heure_fin) ⊆ Projection(Tranche_horaire,heure_debut, heure_fin)


Visite(#creneau=> Creneau.id, #exposition =>Exposition.nom )
CHECK ( Creneau.type == Exposition.type) et Projection(Visite,creneau) = Projection(Creneau,id) et Projection(Visite,exposition) ⊆  Projection(Exposition,nom)








