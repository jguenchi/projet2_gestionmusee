

/* Guide */
INSERT INTO Guide (nom, prenom, date_embauche)
VALUES
('WAICHINE', 'Nathalie', '2018-03-04'),
('COURTIS', 'Christine', '2013-05-23'),
('PERRARD', 'Clémence', '2015-09-11'),
('SINHA', 'Anamika', '2022-12-19')
;

select * from guide;


INSERT INTO Utilisateur_connexion (login, password, status, id_guide ) VALUES
('nathalie', 'nathalie', 'guide', 1),
('admin', 'admin', 'admin', NULL),
('jacques', 'jj', 'employe', NULL)
;

/* Tranche horaire */
INSERT INTO Tranche_horaire (heure_debut, heure_fin)
VALUES
('08:00:00', '09:00:00'),
('10:00:00', '12:00:00'),
('12:00:00', '13:00:00'),
('14:00:00', '15:30:00'),
('16:00:00', '18:00:00')
;




INSERT INTO Creneau (guide, horaire,jour, type, date_debut,date_fin)
VALUES
(1,1,'Lundi','permanent',null,null),
(1,2,'Lundi','permanent',null,null),
(1,3,'Lundi','permanent',null,null),
(1,4,'Lundi','permanent',null,null),
(2,1,'Mardi','exceptionnel','2023-05-13','2023-05-20'),
(2,2,'Mardi','permanent',null,null),
(3,4,'Mercredi','permanent',null,null),
(4,2,'Vendredi','exceptionnel','2023-05-13','2023-05-28'),
(1,4,'Mercredi','exceptionnel','2023-05-13','2023-05-30'),
(4,3,'Vendredi','exceptionnel','2023-05-13','2023-05-29'),
(2,4,'Mardi','exceptionnel','2023-05-13','2023-05-21')
;




/*Exposition*/


INSERT INTO Exposition VALUES
('Chine Empire', 'permanente' ,null, null) ,
('Picasso', 'permanente' ,null, null),
('Sculptures Grecques', 'permanente' ,null, null),
('Arts Egyptien', 'permanente' ,null, null),
('Arts romains', 'permanente' ,null, null),
('Art contemporain', 'permanente' , null, null),
('Peintures espagnol au XIXeme siecle', 'temporaire' ,'2022-02-01', '2023-05-12'),
('Revolution française', 'temporaire' ,'2012-12-05', '2012-12-22'),
('Caravagisme', 'temporaire' ,'2023-01-07', '2024-10-27'),
('Moyen Orient', 'temporaire' ,'2024-01-01', '2024-05-20'),
('Napoleon', 'temporaire' ,'2024-05-01', '2024-07-30')  ;




insert into Visite (creneau,exposition)
VALUES
(1,'Chine Empire'),
(2,'Sculptures Grecques'),
(3,'Arts romains'),
(6,'Arts Egyptien'),
(4,'Picasso'),
(5,'Peintures espagnol au XIXeme siecle'),
(7,'Art contemporain'),
(8,'Revolution française'),
(9,'Caravagisme'),
(10,'Moyen Orient'),
(11,'Napoleon')
;














/* Auteur */
INSERT INTO Auteur(nom_artiste, nom, prenom,date_naissance,date_deces)
VALUES
('Banksy', 'Dupond', 'Barnabé','1956-11-10',NULL),
('L.Vinci', 'Léonard', 'de Vinci','1650-11-06','1716-01-08'),
('Dol', 'Boulanger', 'Marc','1990-11-10',NULL),
('Borne', 'Pichard', 'Manuel','1968-01-24',NULL),
('Bossu', 'Pigasse', 'Jules','2002-02-03',NULL),
('North', 'Minost', 'Yonass','2003-10-03',NULL),
('Balzac', 'Babot', 'Nathan','1930-12-06','1983-03-17'),
('Trouvetout', 'Canard', 'Géo','1350-05-10','1716-01-08'),
('Solstice', 'Equinoxe', 'Jean','1970-11-10',NULL)
;










/*Salle*/
INSERT INTO SALLE (numero, nb_max_personnes)
VALUES
(0, 150),
(1, 100),
(2, 300),
(3, 10),
(4, 50),
(5, 400),
(6, 5),
(7, 22),
(8, 50),
(9, 400),
(10, 35),
(11, 70),
(12, 450),
(13, 56),
(14, 220),
(15, 58),
(16, 80),
(17, 39),
(18, 72)
;


/*Musee*/
INSERT INTO MUSEE(adresse, nom, pays)
VALUES
('1 rue des coquelicots 75001 Paris' ,'Musee Carnavalet', 'France' ),
('150 rue Jeanne d Arc 75008 Paris' ,'Catacombes', 'France' ),
('45 avenue Victor Hugo 75005 Paris' ,'Crypte Archeologique', 'France' ),
('13 rue des lilas 75002 Paris' ,'Petit Palais', 'France' ),
('10 rue des Voltaire 75000 Paris' ,'Maison de Victor Hugo', 'France' ),
('55 rue Saint Anne 75001 Paris' ,'Musee Cognac Jay', 'France' ),
('26 rue des plantes 75000 Paris' ,'Musee Bourdelle', 'France' ),
('66 London' ,'British Museum', 'Angleterre' ),
('150 Venise' ,'Galerie Borghese', 'Italie' ),
('52 Rome' ,'Chateau Saint Ange', 'Italie' );










/*Oeuvre*/
INSERT INTO Oeuvre (titre, date_creation, status, longueur, largeur, hauteur, prix_acquisition, type, auteur, exposition)
VALUES
('Laché d oiseaux', '1976-11-10', 'neutre', 16.5, 20.3, null, 500, 'peinture', 'Banksy', 'Art contemporain' ),
('L empereur', '1810-11-10', 'tierce', 140, 250, null, 1500, 'peinture', 'Dol', 'Napoleon'),
('Mona Lisa', '1690-09-15', 'neutre', 16.5, 20.3, null, 230000, 'peinture', 'L.Vinci', 'Arts romains' ),
('Aphrodite', '1976-11-10', 'neutre', 50.4, 30, 180, 8000, 'sculpture', 'Banksy', 'Sculptures Grecques' ),
('Chasse du roi', '1450-11-03', 'neutre', 16.5, 20.3, null, 1500, 'photographie', 'Trouvetout', 'Arts Egyptien' ),
('Les ménines', '1476-11-10', 'tierce', 160.5, 20.3, null, 500, 'peinture', 'North', 'Peintures espagnol au XIXeme siecle'),
('Unnamed', '1476-11-10', 'neutre', 32, 56.8, null, 0, 'peinture',  'L.Vinci', 'Arts romains' ),
('Le lac', '1500-01-30', 'neutre', 32, 56.8, null, 15000, 'peinture',  'L.Vinci', 'Arts romains' ),
('Hermes', '1220-03-10', 'neutre', 16.5, 20.3, null, 1304, 'sculpture', 'Balzac', 'Sculptures Grecques' ),
('Pluton', '1105-07-03', 'neutre', 16.5, 20.3, null, 0, 'sculpture', 'Balzac', 'Sculptures Grecques' ),
('Laché de cigognes', '1976-11-10', 'tierce', 80, 35, null, 0, 'photographie', 'Solstice', 'Revolution française' ),
('Laché de taureaux', '1976-11-10', 'tierce', 36, 45, null, 0, 'photographie', 'Solstice', 'Caravagisme' ),
('Laché d oiseaux', '1376-01-09', 'tierce', 28, 28, 100, 0, 'sculpture', 'Trouvetout', 'Moyen Orient' ),
('Ecriture carapace tortue', '176-01-09', 'neutre', 28, 60, 50, 0, 'sculpture', 'Trouvetout', 'Chine Empire'),
('Guernica', '1776-11-10', 'neutre', 165, 203, null,7500, 'peinture', 'North', 'Picasso')
;


INSERT INTO Echange(musee,oeuvre,emprunte,date_debut,date_fin)
VALUES
(1, 1, false,'2022-05-16','2024-02-12'),
(2, 2, true,'2021-05-16','2024-04-30'),
(3, 3, false,'2021-05-16','2024-02-10'),
(4, 4, false,'2022-05-21','2024-09-30'),
(5, 5, false,'2020-05-20','2026-07-14'),
(6, 6, true,'2023-05-16','2023-08-18'),
(7, 7, false,'2023-05-16','2025-01-01'),
(8, 8, false,'2023-05-22','2023-09-17'),
(9, 9, false,'2022-05-16','2024-07-06')
;




/* Mise à jour des statuts des oeuvres */

/*
update oeuvre set status = 'tierce' where id in (select oeuvre from Echange where emprunte = True and current_date>Echange.date_fin );
update oeuvre set status = 'neutre' where id in (select oeuvre from Echange where emprunte = False and current_date>Echange.date_fin);


update oeuvre set status = 'emprunte' where id in (select oeuvre from Echange where emprunte = True and current_date<Echange.date_fin );
update oeuvre set status = 'prete' where id in (select oeuvre from Echange where emprunte = False and current_date<Echange.date_fin);
*/





/*Exposition_salle*/
INSERT INTO Exposition_salle (exposition, salle)
VALUES
('Picasso', 0) ,
('Sculptures Grecques', 1) ,
('Sculptures Grecques', 2) ,
('Sculptures Grecques', 3) ,
('Arts Egyptien', 4) ,
('Arts romains', 5) ,
('Picasso', 6) ,
('Revolution française', 7) ,
('Caravagisme', 8) ,
('Moyen Orient', 9) ,
('Napoleon', 10) ,
('Art contemporain', 11),
('Caravagisme', 12) ,
('Revolution française', 13) ,
('Arts Egyptien', 14) ,
('Chine Empire', 15) ,
('Picasso', 16) ,
('Peintures espagnol au XIXeme siecle', 17) ,
('Arts romains', 18)
;
select * from exposition_salle;




INSERT INTO Tableau_explicatif(texte, exposition, salle)
VALUES
('Cette peinture est très ancienne.' , 'Chine Empire', 15 ),
('La Chine de l''Empire regorge d''oeuvres différentes et magnifiques.' , 'Chine Empire',15 ),
('Ces oeuvres ont été très prisées en Europe apres le XVIeme siecle.' , 'Chine Empire',15 ),
('Ce tableau lui a permis de devenir un artiste reconnu.' , 'Picasso',0 ),
('Certaines de ces sculptures sont des reconstitutions.' , 'Sculptures Grecques',1 ),
('Nous pouvons observer une sculpture d''Aphrodite.' , 'Sculptures Grecques',2 ),
('Les Egyptiens ont été les premiers à utiliser l''écriture.' , 'Arts Egyptien',4 ),
('Ces artistes dénoncent la polution dans le monde.' , 'Art contemporain',11 ),
('Goya, Picasso, l''Espagne regorgeait de talents au XIXeme siecle.' , 'Peintures espagnol au XIXeme siecle',17),
('La cruauté de cette période a été illustrée dans l''art.' , 'Revolution française',13 ),
('Ce mouvement ne dura pas longtemps' , 'Caravagisme',8 ),
('On peut observer la maitrise du clair-obscur.' , 'Caravagisme',8 ),
('Goya, Picasso, l''Espagne regorgeait de talents au XIXeme siecle.' , 'Moyen Orient',9),
('Son épopée fut foudroyante.' , 'Napoleon',10 ),
('Nous pouvons observer son couronnement.' , 'Napoleon',10 )
;

select * from tableau_explicatif ;




/* Restauration */
INSERT INTO Restauration (oeuvre, date_debut, date_fin)
VALUES
(1,'2023-05-13','2023-05-20'),
(4,'2023-05-14','2023-05-21'),
(5,'2023-05-15','2023-05-22'),
(8,'2023-05-14','2023-05-25')
;


























