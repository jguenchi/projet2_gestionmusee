

/* Guide */
INSERT INTO Guide (nom, prenom, date_embauche)
VALUES
('WAICHINE', 'Nathalie', '2018-03-04'),
('COURTIS', 'Christine', '2013-05-23'),
('PERRARD', 'Clémence', '2015-09-11'),
('SINHA', 'Anamika', '2022-12-19')
;

select * from guide;


INSERT INTO Utilisateur_connexion (login, password, status, id_guide ) VALUES
('nathalie', 'nathalie', 'guide', 1),
('admin', 'admin', 'admin', NULL),
('jacques', 'jj', 'employe', NULL)
;

/* Tranche horaire */
INSERT INTO Tranche_horaire (heure_debut, heure_fin)
VALUES
('08:00:00', '09:00:00'),
('10:00:00', '12:00:00'),
('12:00:00', '13:00:00'),
('14:00:00', '15:30:00'),
('16:00:00', '18:00:00')
;




INSERT INTO Creneau (guide, horaire,jour, type, date_debut,date_fin)
VALUES
(1,1,'Lundi','permanent',null,null),
(1,2,'Lundi','permanent',null,null),
(1,3,'Lundi','permanent',null,null),
(1,4,'Lundi','permanent',null,null),
(2,1,'Mardi','exceptionnel','2023-05-13','2023-05-20'),
(2,2,'Mardi','permanent',null,null),
(3,4,'Mercredi','permanent',null,null),
(4,2,'Vendredi','exceptionnel','2023-05-13','2023-05-28'),
(1,4,'Mercredi','exceptionnel','2023-05-13','2023-05-30'),
(4,3,'Vendredi','exceptionnel','2023-05-13','2023-05-29'),
(2,4,'Mardi','exceptionnel','2023-05-13','2023-05-21')
;




/* Auteur */
INSERT INTO Auteur(nom_artiste, nom, prenom,date_naissance,date_deces)
VALUES
('Banksy', 'Dupond', 'Barnabé','1956-11-10',NULL),
('L.Vinci', 'Léonard', 'de Vinci','1650-11-06','1716-01-08'),
('Dol', 'Boulanger', 'Marc','1990-11-10',NULL),
('Borne', 'Pichard', 'Manuel','1968-01-24',NULL),
('Bossu', 'Pigasse', 'Jules','2002-02-03',NULL),
('North', 'Minost', 'Yonass','2003-10-03',NULL),
('Balzac', 'Babot', 'Nathan','1930-12-06','1983-03-17'),
('Trouvetout', 'Canard', 'Géo','1350-05-10','1716-01-08'),
('Solstice', 'Equinoxe', 'Jean','1970-11-10',NULL)
;








/*Musee*/
INSERT INTO MUSEE(adresse, nom, pays)
VALUES
('1 rue des coquelicots 75001 Paris' ,'Musee Carnavalet', 'France' ),
('150 rue Jeanne d Arc 75008 Paris' ,'Catacombes', 'France' ),
('45 avenue Victor Hugo 75005 Paris' ,'Crypte Archeologique', 'France' ),
('13 rue des lilas 75002 Paris' ,'Petit Palais', 'France' ),
('10 rue des Voltaire 75000 Paris' ,'Maison de Victor Hugo', 'France' ),
('55 rue Saint Anne 75001 Paris' ,'Musee Cognac Jay', 'France' ),
('26 rue des plantes 75000 Paris' ,'Musee Bourdelle', 'France' ),
('66 London' ,'British Museum', 'Angleterre' ),
('150 Venise' ,'Galerie Borghese', 'Italie' ),
('52 Rome' ,'Chateau Saint Ange', 'Italie' );






/* Mise à jour des statuts des oeuvres */

/*
update oeuvre set status = 'tierce' where id in (select oeuvre from Echange where emprunte = True and current_date>Echange.date_fin );
update oeuvre set status = 'neutre' where id in (select oeuvre from Echange where emprunte = False and current_date>Echange.date_fin);


update oeuvre set status = 'emprunte' where id in (select oeuvre from Echange where emprunte = True and current_date<Echange.date_fin );
update oeuvre set status = 'prete' where id in (select oeuvre from Echange where emprunte = False and current_date<Echange.date_fin);
*/






select * from oeuvre ;

/*Exposition*/
INSERT INTO Exposition VALUES
('Chine Empire',
 'permanente',
 null,
 null,
 '[
 {"numero": 15, "nb_max_personnes": 58, "Tableau_explicatif": [{"texte": "Cette peinture est très ancienne."},
{"texte": "La Chine de l''Empire regorge d''oeuvres différentes et magnifiques."}, {"texte": "Ces oeuvres ont été très prisées en Europe apres le XVIeme siecle."}]}
 ]');


INSERT INTO Exposition VALUES
('Picasso',
 'permanente',
 null,
 null,
'[
 {"numero": 0, "nb_max_personnes": 150, "Tableau_explicatif": [{"texte": "Ce tableau lui a permis de devenir un artiste reconnu."}]}
 ]');

INSERT INTO Exposition VALUES
('Sculptures Grecques',
 'permanente',
 null,
 null,
'[
 {"numero": 1, "nb_max_personnes": 100, "Tableau_explicatif": [{"texte": "Certaines de ces sculptures sont des reconstitutions."}]}, {"numero": 2, "nb_max_personnes": 300, "Tableau_explicatif": [{"texte": "Nous pouvons observer une sculpture d''Aphrodite."}]}
 ]');

INSERT INTO Exposition VALUES
('Arts Egyptien',
 'permanente',
 null,
 null,
 '[
 {"numero": 4, "nb_max_personnes": 50, "Tableau_explicatif": [{"texte": "Les Egyptiens ont été les premiers à utiliser lécriture."}]}
 ]'
);

INSERT INTO Exposition VALUES
('Arts romains',
 'permanente',
 null,
 null,
 null
);

INSERT INTO Exposition VALUES
('Art contemporain',
 'permanente',
 null,
 null,
 '[
 {"numero": 11, "nb_max_personnes": 70, "Tableau_explicatif": [{"texte": "Ces artistes dénoncent la polution dans le monde."}]}
 ]'
);

INSERT INTO Exposition VALUES
('Peintures espagnol au XIXeme siecle',
 'temporaire',
 '2022-02-01',
 '2023-05-12',
 '[
 {"numero": 17, "nb_max_personnes": 39, "Tableau_explicatif": [{"texte": "Goya, Picasso, l''Espagne regorgeait de talents au XIXeme siecle."}]}
 ]'
);
INSERT INTO Exposition VALUES
('Revolution française',
 'temporaire',
 '2012-12-05',
 '2012-12-22',
 '[
 {"numero": 13, "nb_max_personnes": 56, "Tableau_explicatif": [{"texte": "La cruauté de cette période a été illustrée dans l''art."}]}
 ]'
);
INSERT INTO Exposition VALUES
('Caravagisme',
 'temporaire',
 '2023-01-07',
 '2024-10-27',
 '[
 {"numero": 8, "nb_max_personnes": 50, "Tableau_explicatif": [{"texte": "Ce mouvement ne dura pas longtemps."},{"texte": "On peut observer la maitrise du clair-obscur."}]}
 ]'
);
INSERT INTO Exposition VALUES
('Moyen Orient',
 'temporaire',
 '2024-01-01',
 '2024-05-20',
 '[
 {"numero": 9, "nb_max_personnes": 400, "Tableau_explicatif": [{"texte": "Goya, Picasso, l''Espagne regorgeait de talents au XIXeme siecle."}]}
 ]'
);
INSERT INTO Exposition VALUES
('Napoleon',
 'temporaire',
 '2024-05-01',
 '2024-07-30',
  '[
 {"numero": 10, "nb_max_personnes": 35, "Tableau_explicatif": [{"texte": "Son épopée fut foudroyante."},{"texte": "Nous pouvons observer son couronnement."}]}
 ]'
)  ;




insert into Visite (creneau,exposition)
VALUES
(1,'Chine Empire'),
(2,'Sculptures Grecques'),
(3,'Arts romains'),
(6,'Arts Egyptien'),
(4,'Picasso'),
(5,'Peintures espagnol au XIXeme siecle'),
(7,'Art contemporain'),
(8,'Revolution française'),
(9,'Caravagisme'),
(10,'Moyen Orient'),
(11,'Napoleon')
;





/*oeuvre*/

INSERT INTO Oeuvre (titre, date_creation, status, longueur, largeur, hauteur, prix_acquisition, type, auteur, exposition, Restauration)
VALUES
('Laché d oiseaux', '1976-11-10', 'neutre', 16.5, 20.3, null, 500, 'peinture', 'Banksy', 'Art contemporain', '[{"date_debut": "2023-05-13", "date_fin": "2023-05-20"}]'),
('L empereur', '1810-11-10', 'tierce', 140, 250, null, 1500, 'peinture', 'Dol', 'Napoleon', null),
('Mona Lisa', '1690-09-15', 'neutre', 16.5, 20.3, null, 230000, 'peinture', 'L.Vinci', 'Arts romains', null),
('Aphrodite', '1976-11-10', 'neutre', 50.4, 30, 180, 8000, 'sculpture', 'Banksy', 'Sculptures Grecques', '[{"date_debut": "2023-05-15", "date_fin": "2023-05-22"}]'),
('Chasse du roi', '1450-11-03', 'neutre', 16.5, 20.3, null, 1500, 'photographie', 'Trouvetout', 'Arts Egyptien', '[{"date_debut": "2023-05-14", "date_fin": "2023-05-21"}]'),
('Les ménines', '1476-11-10', 'tierce', 160.5, 20.3, null, 500, 'peinture', 'North', 'Peintures espagnol au XIXeme siecle', null),
('Unnamed', '1476-11-10', 'neutre', 32, 56.8, null, 0, 'peinture',  'L.Vinci', 'Arts romains', null),
('Le lac', '1500-01-30', 'neutre', 32, 56.8, null, 15000, 'peinture',  'L.Vinci', 'Arts romains', '[{"date_debut": "2023-05-14", "date_fin": "2023-05-25"}]'),
('Hermes', '1220-03-10', 'neutre', 16.5, 20.3, null, 1304, 'sculpture', 'Balzac', 'Sculptures Grecques', null),
('Pluton', '1105-07-03', 'neutre', 16.5, 20.3, null, 0, 'sculpture', 'Balzac', 'Sculptures Grecques', null),
('Laché de cigognes', '1976-11-10', 'tierce', 80, 35, null, 0, 'photographie', 'Solstice', 'Revolution française', null),
('Laché de taureaux', '1976-11-10', 'tierce', 36, 45, null, 0, 'photographie', 'Solstice', 'Caravagisme', null),
('Laché d oiseaux', '1376-01-09', 'tierce', 28, 28, 100, 0, 'sculpture', 'Trouvetout', 'Moyen Orient', null),
('Ecriture carapace tortue', '176-01-09', 'neutre', 28, 60, 50, 0, 'sculpture', 'Trouvetout', 'Chine Empire', null),
('Guernica', '1776-11-10', 'neutre', 165, 203, null, 7500, 'peinture', 'North', 'Picasso', null);








INSERT INTO Echange(musee,oeuvre,emprunte,date_debut,date_fin)
VALUES
(1, 1, false,'2022-05-16','2024-02-12'),
(2, 2, true,'2021-05-16','2024-04-30'),
(3, 3, false,'2021-05-16','2024-02-10'),
(4, 4, false,'2022-05-21','2024-09-30'),
(5, 5, false,'2020-05-20','2026-07-14'),
(6, 6, true,'2023-05-16','2023-08-18'),
(7, 7, false,'2023-05-16','2025-01-01'),
(8, 8, false,'2023-05-22','2023-09-17'),
(9, 9, false,'2022-05-16','2024-07-06')
;



















